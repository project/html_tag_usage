<?php

namespace Drupal\html_tag_usage\Batch;

use Drupal\Core\TypedData\TranslatableInterface;

/**
 * Provides static methods to be used as batch operations.
 */
class BatchOperations {

  /**
   * Analyzes entities in batches of 50 for HTML tag usage in a field.
   *
   * @param string $entity_type
   *   Entity type id of entities to analyze.
   * @param string[] $bundles
   *   Ids of bundles to analyze.
   * @param string $field_name
   *   Field name of field to analyze.
   * @param mixed $context
   *   Context of batch process.
   */
  public static function analyzeField(string $entity_type, array $bundles, string $field_name, &$context) {
    $batch_size = 50;

    $analyzer = \Drupal::service('html_tag_usage.analyzer');
    $entity_type_manager = \Drupal::service('entity_type.manager');
    $translation = \Drupal::service('string_translation');

    $bundle_key = $entity_type_manager
      ->getDefinition($entity_type)
      ->getKey('bundle');
    $id_key = $entity_type_manager
      ->getDefinition($entity_type)
      ->getKey('id');

    $query = $entity_type_manager->getStorage($entity_type)
      ->getQuery()
      ->accessCheck();
    if (!empty($bundle_key) && !empty($bundles)) {
      $query->condition($bundle_key, $bundles, 'IN');
    }
    $query->sort($id_key);

    if (empty($context['sandbox']) || $entity_type !== $context['sandbox']['entity_type'] || $field_name !== $context['sandbox']['field_name']) {
      $count_query = clone $query;

      $context['sandbox']['entity_type'] = $entity_type;
      $context['sandbox']['field_name'] = $field_name;
      $context['sandbox']['max'] = $count_query->count()->execute();
      $context['sandbox']['progress'] = 0;
      $context['sandbox']['offset'] = 0;
      $context['message'] = $translation->formatPlural($context['sandbox']['max'], 'Analyzing 1 @entity_type entity for HTML tag usage in @field.', 'Analyzing @count entities for HTML tag usage in @field.', [
        '@entity_type' => $entity_type,
        '@field' => $field_name,
      ]);
    }

    $entity_ids = $query
      ->range($context['sandbox']['offset'], $batch_size)
      ->execute();

    $entities = $entity_type_manager
      ->getStorage($entity_type)
      ->loadMultiple($entity_ids);

    foreach ($entities as $entity_id => $entity) {
      $context['results'][] = $translation->translate('Analyzing @field_name for entity with id @entity_id.', [
        '@field_name' => $field_name,
        '@entity_id' => $entity->id(),
      ]);

      if ($entity instanceof TranslatableInterface && $entity->isTranslatable()) {
        foreach ($entity->getTranslationLanguages() as $langcode => $language) {
          $analyzer->analyzeEntityField($entity->getTranslation($langcode), $field_name);
        }
      }
      else {
        $analyzer->analyzeEntityField($entity, $field_name);
      }
    }

    $context['sandbox']['progress'] += count($entity_ids);
    $context['sandbox']['offset'] = $context['sandbox']['offset'] + $batch_size;
    if ($context['sandbox']['progress'] < $context['sandbox']['max']) {
      $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
    }
    else {
      $context['message'] = $translation->translate('Completed HTML tag usage alaysis.');
    }
  }

}
