<?php

namespace Drupal\html_tag_usage;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Component\Utility\Html;
use Drupal\Core\Batch\BatchBuilder;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Database\StatementInterface;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\State\StateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\TypedData\TypedDataInterface;
use Drupal\Core\Url;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\html_tag_usage\Batch\BatchOperations;

/**
 * Analyzes HTML tag usage.
 */
class Analyzer {

  use DependencySerializationTrait;
  use StringTranslationTrait;

  /**
   * Defines state key for Unix timestamp of last report generation.
   *
   * @var string
   */
  const REPORT_LAST_GENERATED_STATE_KEY = 'html_tag_usage.report_timestamp';

  /**
   * The factory for configuration object.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * Constructs a new Analyzer instance.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, Connection $database, EntityTypeManagerInterface $entity_type_manager, EntityFieldManagerInterface $entity_field_manager, MessengerInterface $messenger, StateInterface $state, TimeInterface $time) {
    $this->configFactory = $config_factory;
    $this->database = $database;
    $this->entityTypeManager = $entity_type_manager;
    $this->entityFieldManager = $entity_field_manager;
    $this->messenger = $messenger;
    $this->state = $state;
    $this->time = $time;
  }

  /**
   * Executes a batch process to analyze HTML tag usage.
   */
  public function analyze() {
    $field_types = $this
      ->configFactory
      ->get('html_tag_usage.configuration')
      ->get('field_types');

    $this->database->truncate('html_tag_usage')->execute();
    $this->state->set(self::REPORT_LAST_GENERATED_STATE_KEY, $this->time->getRequestTime());

    $batch_builder = new BatchBuilder();
    $batch_builder->setTitle($this->t('Analyze HTML Tag Usage'));
    foreach ($field_types as $field_type) {
      $operations = $this->analyzeFieldType($field_type);
      foreach ($operations as $operation_arguments) {
        $batch_builder->addOperation([BatchOperations::class, 'analyzeField'], $operation_arguments);
      }
    }

    batch_set($batch_builder->toArray());
    return batch_process(Url::fromRoute('html_tag_usage.report'));
  }

  /**
   * Checks, whether the report has ever been generated.
   *
   * @return bool
   *   Whether the report has ever been generated.
   */
  public function hasReport(): bool {
    return !empty($this->state->get(self::REPORT_LAST_GENERATED_STATE_KEY));
  }

  /**
   * Gets Unix Timestamp of last generation of report.
   *
   * @return int
   *   Unix timestamp.
   */
  public function getReportLastGenerated(): int {
    return (int) $this->state->get(self::REPORT_LAST_GENERATED_STATE_KEY);
  }

  /**
   * Gets tag data by text format.
   *
   * @param bool $include_tag_count
   *   Whether to include the tag count.
   *
   * @return \Drupal\Core\Database\StatementInterface|null
   *   A prepared statement or NULL, if the query is not valid.
   */
  protected function getTagsByFormat(bool $include_tag_count = TRUE): ?StatementInterface {
    $query = $this->database->select('html_tag_usage', 'htu')
      ->fields('htu', ['text_format', 'tag', 'attribute']);
    $query->groupBy('text_format');
    $query->groupBy('tag');
    $query->groupBy('attribute');
    if ($include_tag_count) {
      $query->addExpression('SUM(htu.count)', 'tag_count');
    }
    $query->orderBy('text_format', 'ASC');
    $query->orderBy('tag', 'ASC');
    $query->orderBy('attribute', 'ASC');
    return $query->execute();
  }

  /**
   * Builds a table showing tag counts with attributes by text format.
   *
   * @return array
   *   Render array.
   */
  public function buildTagsWithAttributes(): array {
    $build = [];

    $build['#attached']['library'][] = 'core/drupal.dialog.ajax';

    $rows = [];
    foreach ($this->getTagsByFormat() as $record) {
      $text_format_label = $this->getTextFormatLabel($record->text_format);
      $rows[] = [
        'text_format' => [
          'data' => $text_format_label,
        ],
        'tag' => [
          'data' => [
            '#plain_text' => $record->tag,
          ],
        ],
        'attribute' => [
          'data' => [
            '#plain_text' => $record->attribute,
          ],
        ],
        'count' => [
          'data' => [
            '#type' => 'link',
            '#title' => $record->tag_count,
            '#url' => Url::fromRoute('html_tag_usage.report.inspect', [
              'text_format' => Html::escape($record->text_format),
              'tag' => Html::escape($record->tag),
              'attribute' => Html::escape($record->attribute),
            ]),
          ],
        ],
      ];
    }

    $build['tags_with_attributes_by_format'] = [
      '#type' => 'table',
      '#caption' => $this->t('Tags with attributes by text format'),
      '#header' => [
        [
          'field' => 'text_format',
          'data' => $this->t('Text format'),
        ],
        [
          'field' => 'tag',
          'data' => $this->t('Tag'),
        ],
        [
          'field' => 'attribute',
          'data' => $this->t('Attribute'),
        ],
        [
          'field' => 'count',
          'data' => $this->t('Count'),
        ],
      ],
      '#rows' => $rows,
    ];
    $build['help_star_as_attribute'] = [
      '#type' => 'html_tag',
      '#tag' => 'p',
      '#prefix' => '<small>',
      '#suffix' => '</small>',
      '#value' => $this->t("Tags listing <code>*</code> as attribute value don't have any HTML attributes."),
    ];

    return $build;
  }

  /**
   * Gets the title for a dialog that would display the inspection table.
   *
   * @param string $text_format
   *   The text format id.
   * @param string $tag
   *   The tag name.
   * @param string $attribute
   *   The attribute name. Use '*' to get results for tags without attributes.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   The title.
   */
  public function getInspectionTitle(string $text_format, string $tag, string $attribute): TranslatableMarkup {
    return $this->t('Inspect <code>@tag[@attribute]</code> for @text_format', [
      '@text_format' => $this->getTextFormatLabel($text_format, FALSE),
      '@tag' => $tag,
      '@attribute' => $attribute,
    ]);
  }

  /**
   * Builds render array for a table to inspect a tag.
   *
   * @param string $text_format
   *   The text format id.
   * @param string $tag
   *   The tag name.
   * @param string $attribute
   *   The attribute name. Use '*' to get results for tags without attributes.
   *
   * @return array
   *   Renderable array.
   */
  public function buildInspectionTable(string $text_format, string $tag, string $attribute): array {
    $header = [
      [
        'field' => 'entity_type',
        'data' => $this->t('Entity Type'),
        'sort' => 'asc',
      ],
      [
        'field' => 'entity_id',
        'data' => $this->t('Entity Id'),
        'sort' => 'asc',
      ],
      [
        'field' => 'langcode',
        'data' => $this->t('Language'),
      ],
      [
        'field' => 'field_name',
        'data' => $this->t('Field'),
      ],
      [
        'field' => 'entity',
        'data' => $this->t('Entity'),
      ],
      [
        'field' => 'tag_count',
        'data' => $this->t('Count'),
      ],
    ];

    $query = $this->database->select('html_tag_usage', 'htu')
      ->extend('\\Drupal\\Core\\Database\\Query\\PagerSelectExtender')
      ->extend('\\Drupal\\Core\\Database\\Query\\TableSortExtender');
    /** @var \Drupal\Core\Database\Query\PagerSelectExtender $query */
    $query->limit(15);
    /** @var \Drupal\Core\Database\Query\TableSortExtender $query */
    $query->orderByHeader($header);
    /** @var \Drupal\Core\Database\Query\SelectInterface $query */
    $query->fields('htu', ['entity_type', 'entity_id', 'langcode', 'field_name']);
    $query->condition('htu.text_format', $text_format);
    $query->condition('htu.tag', $tag);
    $query->condition('htu.attribute', $attribute);
    $query->addExpression('SUM(htu.count)', 'tag_count');
    $query->groupBy('htu.entity_type');
    $query->groupBy('htu.entity_id');
    $query->groupBy('htu.langcode');
    $query->groupBy('htu.field_name');

    $results = $query->execute();
    $rows = [];
    foreach ($results as $record) {
      /** @var \Drupal\Core\Entity\FieldableEntityInterface $entity */
      $entity = $this->entityTypeManager
        ->getStorage($record->entity_type)
        ->load($record->entity_id);

      $entity_label = $entity->getEntityType()->hasLinkTemplate('edit-form') ? $entity->toLink($entity->label(), 'edit-form')->toRenderable() : $entity->label();
      if ($entity->getEntityType()->id() === 'paragraph') {
        // Link to parent entity edit form for paragraphs.
        $paragraph = $entity;
        while ($paragraph->getEntityType()->id() === 'paragraph' && $parent_entity = $paragraph->getParentEntity()) {
          $paragraph = $parent_entity;
          $entity_label = $parent_entity->hasLinkTemplate('edit-form') ? $parent_entity->toLink($entity->label(), 'edit-form')->toRenderable() : $entity->label();
        }
      }

      $rows[] = [
        'entity_type' => [
          'data' => [
            '#plain_text' => $entity->getEntityType()->getSingularLabel(),
          ],
        ],
        'entity_id' => [
          'data' => [
            '#plain_text' => $entity->id(),
          ],
        ],
        'langcode' => [
          'data' => $entity->language()->getName(),
        ],
        'field' => [
          'data' => [
            '#plain_text' => $entity
              ->get($record->field_name)
              ->getFieldDefinition()
              ->getLabel(),
          ],
        ],
        'label' => [
          'data' => $entity_label,
        ],
        'tag_count' => [
          'data' => [
            '#plain_text' => $record->tag_count,
          ],
        ],
      ];
    }

    $build['table_for_inspection'] = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
    ];
    $build['table_for_inspection_pager'] = [
      '#type' => 'pager',
      '#route_name' => '<current>',
    ];

    return $build;
  }

  /**
   * Gets the label for an id of a text format.
   *
   * @param string $text_format
   *   Text format id.
   * @param bool $linked
   *   Whether to link the label to the text format's edit form.
   *
   * @return mixed
   *   Either translatable markup for a text format label or a renderable
   *   array for the label linked to the text format's edit form.
   */
  protected function getTextFormatLabel(string $text_format, $linked = TRUE) {
    $text_format_entity = $this->entityTypeManager
      ->getStorage('filter_format')
      ->load($text_format);
    if (!$text_format_entity) {
      // Some field values use an invalid text format. This shouldn't happen,
      // but it does in practice, e.g. for sites with migrated content.
      return $this->t('Invalid text format id <code>@text_format_id</code>', [
        '@text_format_id' => $text_format,
      ]);
    }
    $text_format_link = $text_format_entity->toLink(NULL, 'edit-form');
    return $linked && $text_format_link->getUrl()->access() ? $text_format_link->toRenderable() : $text_format_entity->label();
  }

  /**
   * Builds configuration for HTML filter for all found text formats.
   *
   * For each text format in use, this method will generate a configuration to
   * be used with the HTML filter that would not filter any elements or
   * attributes currently in use on your site. Note that using the configuration
   * as is might be insecure.
   *
   * The results are returned as a render array for display.
   *
   * @return array
   *   Render array.
   */
  public function buildHtmlFilterConfiguration() {
    $build = [];

    $map = [];
    foreach ($this->getTagsByFormat(FALSE) as $record) {
      if ($record->attribute === '*') {
        $map[$record->text_format][$record->tag] = [];
        continue;
      }
      $map[$record->text_format][$record->tag][] = $record->attribute;
    }

    $filter_configuration = [];
    foreach ($map as $text_format => $tags) {
      $tags = array_keys($tags);

      $config = [];

      foreach ($tags as $tag) {
        $attributes = !empty($map[$text_format][$tag]) ? $map[$text_format][$tag] : [];
        $config[$tag] = '<' . trim($tag . ' ' . implode(' ', $attributes)) . '>';
      }

      if (empty($config)) {
        continue;
      }

      $label = $this->getTextFormatLabel($text_format);
      $filter_configuration[$text_format] = [
        'headline' => [
          '#type' => 'html_tag',
          '#tag' => 'h3',
          'label' => is_array($label) ? $label : [
            '#markup' => $label,
            '#allowed_tags' => ['code'],
          ],
        ],
        'configuration' => [
          '#prefix' => '<code>',
          '#plain_text' => implode(' ', $config),
          '#suffix' => '</code>',
        ],
      ];
    }

    $build['text_filter_configuration'] = [
      'headline' => [
        '#type' => 'html_tag',
        '#tag' => 'h2',
        '#value' => $this->t('HTML Filter Configuration'),
      ],
      'description' => [
        '#type' => 'html_tag',
        '#tag' => 'p',
        '#value' => $this->t('Use the following HTML filter configuration to render all HTML elements currently in use. Note: It may be insecure to use the generated configuration as is. You are encouraged to review it before use.'),
      ],
    ] + $filter_configuration;

    return $build;
  }

  /**
   * Gets batch operation parameters to analyze fields of a certain type.
   *
   * This method gets all fields for a field type and sets up parameter arrays
   * suitable to be passed to a batch operation callback, which is then used to
   * perform the analysis for a certain field.
   *
   * @param string $field_type
   *   Id of field type to analyze.
   *
   * @return array
   *   Array of parameter arrays for
   *   \Drupal\html_tag_usage\BatchOperations::analyzeFieldType().
   */
  protected function analyzeFieldType(string $field_type): array {
    $operations = [];

    $field_map = $this->entityFieldManager->getFieldMapByFieldType($field_type);
    foreach ($field_map as $entity_type => $fields) {
      if (!$this->entityTypeManager->hasDefinition($entity_type)) {
        // There is field data for an entity type that doesn't exist. This
        // shouldn't happen, but it does in practice, e.g. for sites with
        // migrated content.
        $this->messenger->addWarning($this->t('Analysis of field data for invalid entity type id <code>@entity_type_id</code> has been skipped.', [
          '@entity_type_id' => $entity_type,
        ]));
        continue;
      }
      foreach ($fields as $field_name => $field_config) {
        $operations[] = [
          $entity_type,
          $field_config['bundles'],
          $field_name,
        ];
      }
    }

    return $operations;
  }

  /**
   * Analyze HTML tag usage for a field of an entity.
   *
   * Analyzes a field of an entity for HTML tag usage and writes the results
   * into the database.
   *
   * @param \Drupal\Core\Entity\FieldableEntityInterface $entity
   *   The entity to analyze.
   * @param string $field_name
   *   The field to analyze.
   */
  public function analyzeEntityField(FieldableEntityInterface $entity, string $field_name) {
    if (!$entity->hasField($field_name)) {
      return;
    }
    $field_item_list = $entity->get($field_name);
    if ($field_item_list->isEmpty()) {
      return;
    }
    foreach ($field_item_list as $delta => $field_item) {
      $this->analyzeEntityFieldItem($entity, $field_name, $delta, $field_item);
    }
  }

  /**
   * Analyze HTML tag usage for a field item of a field of an entity.
   *
   * Analyzes a field item value of a field of an entity for HTML tag usage and
   * writes the results into the database.
   *
   * @param \Drupal\Core\Entity\FieldableEntityInterface $entity
   *   The entity to analyze.
   * @param string $field_name
   *   The name of the field to analyze.
   * @param int $delta
   *   The delta of the field item.
   * @param \Drupal\Core\TypedData\TypedDataInterface $field_item
   *   The field item to analyze.
   */
  protected function analyzeEntityFieldItem(FieldableEntityInterface $entity, string $field_name, int $delta, TypedDataInterface $field_item) {
    $tags = [];
    $seen = [];

    $value = $field_item->getValue();
    $row_base = [
      'entity_type' => $entity->getEntityTypeId(),
      'entity_id' => $entity->id(),
      'langcode' => $entity->language()->getId(),
      'field_name' => $field_name,
      'delta' => $delta,
      'text_format' => $value['format'],
    ];

    $dom = Html::load($value['value']);
    foreach ($dom->getElementsByTagName('*') as $element) {
      $tag = $element->tagName;
      $tag_html = $element->ownerDocument->saveXML($element);
      if (!isset($seen[$tag]) && $this->isParserOverhead($tag, $tag_html)) {
        $seen[$tag] = TRUE;
        continue;
      }
      if (!$element->hasAttributes()) {
        if (!isset($tags[$tag]['*'])) {
          $tags[$tag]['*'] = 1;
          continue;
        }
        $tags[$tag]['*'] = $tags[$tag]['*'] + 1;
        continue;
      }

      foreach ($element->attributes as $attribute) {
        $attribute_name = $attribute->nodeName;
        if (!isset($tags[$tag][$attribute_name])) {
          $tags[$tag][$attribute_name] = 1;
          continue;
        }
        $tags[$tag][$attribute_name] = $tags[$tag][$attribute_name] + 1;
      }
    }

    $rows = [];

    foreach ($tags as $tag => $attributes) {
      foreach ($attributes as $attribute => $count) {
        $query = $this->database->insert('html_tag_usage');
        $query->fields($row_base + [
          'tag' => $tag,
          'attribute' => $attribute,
          'count' => $count,
        ]);
        $query->execute();
      }
    }

    return $tags;
  }

  /**
   * Checks, whether a tag is parser overhead.
   *
   * The \DOMDocument returned by \Drupal\Component\Utility\Html::load() will
   * wrap the HTML to load as a \DOMDocument inside some additional markup so
   * that it is a valid HTML document. Since we don't want to count these tags,
   * we need to skip them.
   *
   * Note: Since the HTML value given to \Drupal\Component\Utility\Html::load()
   * and the output of \DOMDocument::save() is not guaranteed to be equal, it is
   * not possible to accurately check html and body tags. Also, there might be
   * head and meta tags in the analyzed output whose contents exactly match the
   * wrapper HTML used by \Drupal\Component\Utility\Html::load(). Since the
   * wrapper HTML elements should be matched first (they are at the beginning of
   * the \DOMDocument, callers should not call this method again for the same
   * tag name once this method has identified a tag as parser overhead.
   *
   * @param string $tag
   *   The tag to check.
   * @param string $tag_html
   *   The full HTML of the tag.
   *
   * @return bool
   *   Whether a tag is parser overhead.
   *
   * @see \Drupal\Component\Utility\Html::load()
   */
  protected function isParserOverhead(string $tag, string $tag_html): bool {
    if (!in_array($tag, ['html', 'head', 'body', 'meta'])) {
      return FALSE;
    }
    if ($tag === 'meta' && $tag_html === '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />') {
      return TRUE;
    }
    if ($tag === 'head' && $tag_html === '<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>') {
      return TRUE;
    }
    if ($tag === 'body') {
      return TRUE;
    }
    if ($tag === 'html') {
      return TRUE;
    }

    return FALSE;
  }

}
