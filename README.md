# CONTENTS OF THIS FILE

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

# INTRODUCTION

The *HTML Tag Usage* module analyzes the contents of formatted text fields on your site and generates a report that lists all HTML tags and HTML attributes in use for each text format. You may inspect a tag/attribute combination to get a list of entities that use the tag/attribute combination.

Potential use cases for this module might include, but are not limited to:

* You just took over maintenance of a Drupal installation from somebody else. Upon inspecting the text formats of the Drupal installation, you notice that none of them use the HTML filter and will just return the HTML as is. Now you would like to make the setup more secure by limiting HTML tags available for the text formats to what is actually needed, but you don't want to accidentally "alter" the content entered by the editors of your Drupal installation, so you need to know what tags and attributes are actually in use.
* Your data protection officer wants you to remove any iframes that embed content from a popular video portal to maintain GDPR compliance. Upon inspecting the text formats on your site, you notice that some of them allow usage of iframe tags. You don't know, whether they are actually in use. You don't know, if a migration to video Media entities would best be done manually or if the number of iframe tags warrants writing a custom module to perform the migration. If you decided to perform a manual migration, you would need a list of entities that use iframe tags.

Note that the module is best used in a development environment and not in production. Depending on the size of your Drupal installation, the module may generate a huge database table to store results when analyzing the content of your site. Therefore, it is recommended to uninstall it as soon as you no longer need the report.

For a full description of the module, visit the project page:
https://www.drupal.org/html_tag_usage

To submit bug reports and feature suggestions, or to track changes:
https://www.drupal.org/node/add/project-issue/html_tag_usage

# REQUIREMENTS

This module depends on the `text` module provided by Drupal Core.

# INSTALLATION

 * Install as you would normally install a contributed Drupal module. See:
   https://www.drupal.org/documentation/install/modules-themes/modules-8
   for further information.

# CONFIGURATION

 * Go to the configuration page of the module at Configuration > Development > HTML Tag Usage.
 * Configure the *field types* of the formatted text fields, whose contents you want to be analyzed. By default, all formatted field types provided by the `text` module are selected for analysis.
 * Go to the report page of the module at Reports > HTML Tag Usage.
 * Click on the *Generate report* link to generate the report. A batch process will analyze all fields of the selected field types. Once the batch process is complete, you will be redirected to the generated report. You may regenerate the report at any time.
 * Make sure to review the permissions provided by the module. There is one permission to administer the configuration, one permission to generate reports and one permission to view reports.

# MAINTAINERS

Current maintainers:
 * Patrick Fey (feyp) - https://drupal.org/u/feyp

This project has been partly sponsored by:
 * [werk21 GmbH](http://www.werk21.de)
