<?php

namespace Drupal\html_tag_usage\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Field\FieldTypePluginManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form to configure HTML tag usage settings.
 */
class ConfigurationForm extends ConfigFormBase {

  /**
   * The field type plugin manager.
   *
   * @var \Drupal\Core\Field\FieldTypePluginManagerInterface
   */
  protected $fieldTypeManager;

  /**
   * Constructs a new HTML Tag Usage configuration form.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Field\FieldTypePluginManagerInterface $field_type_manager
   *   The field type manager.
   */
  public function __construct(ConfigFactoryInterface $config_factory, FieldTypePluginManagerInterface $field_type_manager) {
    parent::__construct($config_factory);
    $this->fieldTypeManager = $field_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('plugin.manager.field.field_type')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'html_tag_usage.configuration',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'html_tag_usage_configuration_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('html_tag_usage.configuration');

    $form['field_types'] = [
      '#type' => 'select',
      '#title' => t('Field Types'),
      '#options' => $this->getFieldTypeOptions(),
      '#description' => t('Select the field types, whose contents you want to be analyzed when generating HTML tag usage reports.'),
      '#size' => 10,
      '#multiple' => TRUE,
      '#required' => TRUE,
      '#default_value' => $config->get('field_types'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * Gets options for field types form element.
   *
   * @return array
   *   Field type labels keyed by field type ids.
   */
  protected function getFieldTypeOptions(): array {
    $options = [];

    foreach ($this->fieldTypeManager->getDefinitions() as $definition) {
      $options[$definition['id']] = $definition['label'];
    }

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $field_types = array_filter($form_state->getValue('field_types'));
    $this->config('html_tag_usage.configuration')
      ->set('field_types', $field_types)
      ->save();
  }

}
