<?php

namespace Drupal\html_tag_usage\Controller;

use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Link;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Drupal\html_tag_usage\Analyzer;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Provides a controller to display and generate the HTML tag usage report.
 */
class ReportController implements ContainerInjectionInterface {

  use DependencySerializationTrait;
  use StringTranslationTrait;

  /**
   * The HTML tag usage analyzer.
   *
   * @var \Drupal\html_tag_usage\Analyzer
   */
  protected $analyzer;

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * Constructs a new ReportController instance.
   *
   * @param \Drupal\html_tag_usage\Analyzer $analyzer
   *   The HTML tag usage analyzer.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter.
   */
  public function __construct(Analyzer $analyzer, DateFormatterInterface $date_formatter) {
    $this->analyzer = $analyzer;
    $this->dateFormatter = $date_formatter;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('html_tag_usage.analyzer'),
      $container->get('date.formatter')
    );
  }

  /**
   * Builds the HTML tag usage report.
   *
   * @return array
   *   A render array with audit results, grouped by severity.
   */
  public function buildReport() {
    $build = [];

    $analyze_url = Url::fromRoute('html_tag_usage.analyze');
    if (!$this->analyzer->hasReport()) {
      $build['never'] = [
        '#type' => 'html_tag',
        '#tag' => 'p',
        '#value' => $this->t('The HTML tag usage report has never been generated. @generate_report', [
          '@generate_report' => $analyze_url->access() ? $this->t('<a href=":url">Generate report</a>', [
            ':url' => $analyze_url->toString(),
          ]) : $this->t("You don't have permission to generate the report."),
        ]),
      ];

      return $build;
    }

    $build['last_generated'] = [
      '#type' => 'html_tag',
      '#tag' => 'p',
      '#value' => $this->t('This HTML tag usage report has been generated at %date.', [
        '%date' => $this->dateFormatter->format($this->analyzer->getReportLastGenerated()),
      ]),
    ];

    $build['link'] = Link::fromTextAndUrl($this->t('Regenerate report'), $analyze_url)->toRenderable();
    $build['link'] += [
      '#access' => $analyze_url->access(),
      '#prefix' => '<p>',
      '#suffix' => '</p>',
    ];

    return $build + $this->analyzer->buildTagsWithAttributes() + $this->analyzer->buildHtmlFilterConfiguration();
  }

  /**
   * Analyze field types for tag usage.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response.
   */
  public function analyze(): RedirectResponse {
    return $this->analyzer->analyze();
  }

  /**
   * Gets title of dialog to inspect tags.
   *
   * @param string $text_format
   *   The text format id.
   * @param string $tag
   *   The tag name.
   * @param string $attribute
   *   The attribute name. Use '*' to get results for tags without attributes.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   Dialog title.
   */
  public function getDialogTitle(string $text_format, string $tag, string $attribute): TranslatableMarkup {
    return $this->analyzer->getInspectionTitle($text_format, $tag, $attribute);
  }

  /**
   * Gets content of dialog to inspect tags.
   *
   * @param string $text_format
   *   The text format id.
   * @param string $tag
   *   The tag name.
   * @param string $attribute
   *   The attribute name. Use '*' to get results for tags without attributes.
   *
   * @return array
   *   Render array.
   */
  public function getDialogContent(string $text_format, string $tag, string $attribute): array {
    return $this->analyzer->buildInspectionTable($text_format, $tag, $attribute);
  }

}
